#!/bin/bash

#s="\E[37m ▣"
s="\E[37m||"
e="\E[31m::"



area[0]="01101001100110010110"
area[1]="00100110001000100111"
area[2]="01101001001001001111"
area[3]="01101001001010010110"
area[4]="00100110101011110010"
area[5]="11111000111000011110"
area[6]="01101000111010010110"
area[7]="11110001001001001000"
area[8]="01101001011010010110"
area[9]="01101001011100010110"
area[10]="01100110000001100110"
area[11]="00000000000000000000"

function drawTime {
	
	mask="${area[${1:0:1}]}${area[${1:1:1}]}${area[10]}${area[${2:0:1}]}${area[${2:1:1}]}${area[10]}${area[${3:0:1}]}${area[${3:1:1}]}"
	nSymbols=8
	for ((y=0; y<5; y++ ))
	do
		tput cup $(( $y + 9 )) 4
		for ((z=0; z<$nSymbols; z++ ))
		do
			for ((x=0; x<4; x++ ))
			do
				ss=${mask:$y*4+$x+$z*20:1}
				if [[ $ss = "1" ]]
				then
					echo -e -n "$s"
				else
					echo -e -n "$e"
				fi
			done
			echo -n " "
		done
	done
	tput sgr0
}
function clock {
    drawTime $1 $2 $3
}

clear
ORIG=`stty -g`
stty -echo
while true; do
tput cup 4 30
tput setaf 1
tput setab 6
tput bold
echo "Pulse Computer Consulting"
tput sgr0
tput cup 17 55
tput setaf 3
echo "Designed by Beetle"
clock `date "+%H %M %S"`

sleep 1
tput sgr0
done
